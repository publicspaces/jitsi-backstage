# Jitsi Backstage

JITSI server configuration for remote speakers at the PublicSpaces Conference.

https://conference.publicspaces.net

## About

These are the config files for the Jitsi video conferencing software used for the remote speakers. A number of adjustments have been made in the config files so that Jitsi can be used properly on a large screen in the conference room.

Adjustments have been made:
- Removal of logo
- Removing call timer and connection settings button
- No 'self' display by default
- No default zooming to get best image quality
